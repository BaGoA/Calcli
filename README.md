# Calcli [![Build Status](https://travis-ci.org/BaGoA/Calcli.svg?branch=main)](https://travis-ci.org/BaGoA/Calcli)

Calcli is a simple C++ command line calculator. 

## Build
Build of Calcli project is made by [CMake](https://cmake.org/) associate to [Ninja](https://ninja-build.org/). 

To build Calcli project, you can use script build.py in ci folder like this:

	./build.py --type debug or ./build.py --type release

To launch Calcli units tests, you can use script tests.py in ci folder like this:

	./test.py --type debug or ./test.py --type release

## Licensing
Calcli is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License.
